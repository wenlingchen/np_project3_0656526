#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sstream>
#include <fcntl.h>
#include <sys/resource.h>

#define MAXPATH 200
#define MAXLINE 1024

using namespace std;
int readline(int fd, char * ptr,int maxlen)
{
  int n,rc;
  char c;
  for(n=1;n<maxlen;n++)
  {
    if ((rc=read(fd,&c,1))==1)
    {
      *ptr++=c;
      if(c=='\n'){break;}
    }
    else if(rc==0)
    {
      if(n==1){return(0);}  //EOF, no data read
      else {break;} //EOF, some data was read
    }
    else
    {return(-1);}
  }
  *ptr=0;
  return(n);	//return length
}

int main(int argc, char *argv[])
{
	int port = atoi(argv[1]);	//port number	
	int sockfd,newsockfd,clilen,childpid;
  	struct sockaddr_in cli_addr,serv_addr;
  	/////////////Open a TCP socket (ppt p.7)///////////////
	if ((sockfd=socket(AF_INET,SOCK_STREAM,0))<0)
	{cout<<"server: can't open stream socket."<<endl;}
	////////////////////Bind (ppt p.8)////////////////////
	bzero((char *)&serv_addr,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=htonl(INADDR_ANY);
	serv_addr.sin_port=htons(port);

	//port reuse
	int enable=1;
	setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int));

  	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
	{cout<<"server: can't bind local address."<<endl;}
	////////////////////Listen (ppt p.8)////////////////////
	listen(sockfd,5);	//backlog=5, the waiting amount in queue
	cout<<"Server is opened..."<<endl;

	////////////////////Accept (ppt p.9)////////////////////
  	while(1)
  	{
	    //accept client (newsockfd)
	    clilen=sizeof(cli_addr);
	    newsockfd=accept(sockfd,(struct sockaddr *)&cli_addr,(socklen_t*)&clilen);  //(socklen_t*)
	    if(newsockfd<0) {cout<<"server: accept error..."<<endl;}
	    if((childpid=fork())<0) {cout<<"server: fork error..."<<endl;}
	    else if(childpid==0)	//child process of main: Accept correct
	    {
	    	close(sockfd);	//close original socket
			char line[MAXLINE]={0};	//MAX input lenth:1024 char
			char request[MAXLINE]={0};
			char request_copy[MAXLINE]={0};
			char cgi_name[MAXLINE]={0};
			char cgi_path[MAXLINE]={0};
			char page_name[MAXLINE]={0};
			char query_string[MAXLINE];
			memset(query_string,0,sizeof(query_string));

			read(newsockfd,line,MAXLINE);
			char * t=strtok(line,"\r\n");

			strncpy(request,t,strlen(t));
			strcpy(request_copy,request);
			cout<<"======================"<<endl;
			cout<<request_copy<<endl;

			string method,request_str,http_ver;
			stringstream ss;
			ss<<request_copy;
			ss>>method>>request_str>>http_ver;

			cout<<"request_str="<<request_str<<endl;

			if(request_str.find(".cgi")!=-1)	//cgi
			{
				int pos=request_str.find("?");
				if(pos>=0)	// GET *.cgi?a=aa&b=bb HTTP/1.1
				{
					string str1=request_str.substr(1,pos-1);
					string str2=request_str.substr(pos+1);
					strcpy(cgi_name,str1.c_str());
					strcpy(query_string,str2.c_str());
					cout<<"cgi_name="<<cgi_name<<endl;
					cout<<"query_string="<<query_string<<endl;
				}
				else	// GET *.cgi HTTP/1.1
				{
					string str1=request_str.substr(1,request_str.length());
					strcpy(cgi_name,str1.c_str());
					strcpy(query_string,"");
					cout<<"cgi_name="<<cgi_name<<endl;
					cout<<"query_string="<<query_string<<endl;
				}
				//setenv
				setenv("QUERY_STRING",query_string,1);
				setenv("CONTENT_LENGTH","ANY",1);
				setenv("REQUEST_METHOD",method.c_str(),1);
				setenv("SCRIPT_NANE","ANY",1);
				setenv("REMOTE_HOST","ANY",1);
				setenv("REMOTE_ADDR","ANY",1);
				setenv("AUTH_TYPE","ANY",1);
				setenv("REMOTE_USER","ANY",1);
				setenv("REMOTE_IDENT","ANY",1);
				getcwd(cgi_path,MAXPATH);	//get current directory path
				strcat(cgi_path,"/cgi/");	//add "/cgi/"
				strcat(cgi_path,cgi_name);
				cout<<"cgi_path="<<cgi_path<<endl;
				//status
				char status[MAXLINE] = {0};
				strcpy(status,"HTTP/1.1 200 OK\r\n");
				write(newsockfd,status,strlen(status));
				//fork and exec
				int cgi_childpid=fork();
				if(cgi_childpid==-1)
				{
					cout<<"cgi_childpid fork error."<<endl;
					exit(1);
				}
				else if(cgi_childpid==0)	//child
				{
					cout<<"cgi_fork success.\n";
					//dup2(newsockfd,2);	//STDERR
					dup2(newsockfd,1);	//STDOUT
					if((execl(cgi_path,cgi_name,(char *)NULL))!=0)
					{
						cout<<"execl error"<<endl;
						exit(1);
					}
				}
				else	//parent
				{
					close(newsockfd);
					//wait(NULL);
				}
			}
			else if(request_str.find(".htm")!=-1)	//htm
			{
				string str1=request_str.substr(1,request_str.length());
				strcpy(page_name,str1.c_str());
				strcpy(query_string,"");
				cout<<"page_name="<<page_name<<endl;
				//status
				char status[MAXLINE];
				strcpy(status,"HTTP/1.1 200 OK\r\n");
				strcat(status,"Content-Type: text/html\n\n");
				write(newsockfd,status,strlen(status));
				int filefd;
				char file_content[MAXLINE];
				filefd=open(page_name,O_RDONLY);
				while(readline(filefd,file_content,MAXLINE))
				{
					write(newsockfd,file_content,strlen(file_content));
				}

			}


	    	exit(0);

	    }	//end of child
	    close(newsockfd);
		//parent process of main
	    //wait(NULL);	//kill zombie----change:SIGNAL
	}	//end of while(1)

	return 0;
}