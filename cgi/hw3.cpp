#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <sstream>
#include <errno.h>
#define MAXLINE	1024
#define MAXSER	5
#define F_CONNECTING	0
#define F_READING	1
#define F_WRITING	2
#define F_DONE	3
using namespace std;
int gSc[MAXSER];

int readline(int fd,char *ptr,int maxlen,int i)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;	
			if(c==' '&& *(ptr-2) =='%'){ gSc[i] = 1; break; }
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;
		}
		else
		{
			return(-1);
		}
	}
	return(n);
}

int main(int argc, char *argv[])
{
	char query_string[MAXLINE];
	char host[MAXSER][MAXLINE];
	char port[MAXSER][MAXLINE];
	char filename[MAXSER][MAXLINE];
	int csock[MAXSER];
	struct sockaddr_in csock_sin[MAXSER];
	int status[MAXSER];
	int filefd[MAXSER];

	memset(query_string,'\0',MAXLINE);
	for(int i=0;i<MAXSER;i++)
	{
		memset(host[i],'\0',MAXLINE);
		memset(port[i],'\0',MAXLINE);
		memset(filename[i],'\0',MAXLINE);
	}
	int nfds;
	fd_set rfds;
	fd_set wfds;
	fd_set rs;
	fd_set ws;

	for(int i=0;i<MAXSER;i++)	//initailize
	{
		csock[i]=-1;
		filefd[i]=-1;
		gSc[i]=0;
	}
	strcpy(query_string,getenv("QUERY_STRING"));
	string query_str;
	stringstream ss;
	ss<<query_string;
	ss>>query_str;
	int index;
	while(1)	//parse the query_string
	{
		int pos=query_str.find("&");
		string tmp=query_str.substr(0,pos);
		string v_str=tmp.substr(tmp.find("=")+1,tmp.length());
		switch(tmp[0])
		{
			case 'h':
				strcpy(host[index],v_str.c_str());
				break;
			case 'p':
				strcpy(port[index],v_str.c_str());
				break;
			case 'f':
				strcpy(filename[index],v_str.c_str());
				index++;
				break;
			default:
				break;
		}
		if(pos!=-1) {query_str.replace(0,pos+1,"");}
		else {break;}
	}
	//Print HTML header
	cout<<"Content-type: text/html\r\n\n";
	cout<<"<html>\r\n";
    cout<<"<head>\r\n";
    cout<<"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n";
    cout<<"<title>Network Programming Homework 3</title>\r\n";
    cout<<"</head>\r\n";
    cout<<"<body bgcolor=#336699>\r\n";
    cout<<"<font face=\"Courier New\" size=2 color=#FFFF99>\r\n";
    cout<<"<table width=\"800\" border=\"1\">\r\n";
    cout<<"<tr>\r\n";
    for(int i=0;i<MAXSER;i++)
    {
    	cout<<"<td>"<<host[i]<<"</td>\r\n";
    }
    cout<<"</tr>\r\n";
    cout<<"<tr>\r\n";
    for(int i=0;i<MAXSER;i++)
    {
    	cout<<"<td valign=\"top\" id=\"m"<<i<<"\"></td>\r\n";
    }
    cout<<"</tr>\r\n";
    cout<<"</table>\r\n";
    fflush(stdout);

    //Create socket, getaddrinfo, connect and NONBLOCK
	for(int i=0;i<MAXSER;i++)
	{
		//Open file
		if(strlen(filename[i])!=0)
		{
			/*
			char filepath[MAXLINE];
			strcpy(filepath,"./");
			strcat(filepath,filename[i]);
			fprintf(stderr, "filepath: %s\n", filepath);
			filefd[i]=open(filepath,O_RDONLY);
			*/
			filefd[i]=open(filename[i],O_RDONLY);
			fprintf(stderr,"%s:%d\n",filename[i],filefd[i]);
		}
		if(filefd[i]!=-1)	//open succese
		{
			struct hostent *he;

			struct addrinfo hints, *res, *k;
			memset(&hints,0,sizeof(hints));
			if(host[i]!="" && port[i]!="" && filename[i]!="")
			{
				/*
				if((he=gethostbyname(host[i]))==NULL)
				{
					fprintf(stderr, "gethostbyname error\n");
					exit(1);
				}
				else
				{
					csock[i]=socket(AF_INET,SOCK_STREAM,0);
					bzero(&csock_sin[i],sizeof(csock_sin[i]));
					csock_sin[i].sin_family=AF_INET;
					csock_sin[i].sin_addr=*((struct in_addr *)he->h_addr);
					csock_sin[i].sin_port=htons((u_short)atoi(port[i]));
				}
				*/
				
				hints.ai_family=AF_INET;
				hints.ai_socktype=SOCK_STREAM;
				if(getaddrinfo(host[i],port[i],&hints,&res)!=0)
				{
					perror("getaddrinfo error");
					exit(1);
				}
				for(k=res;k!=NULL;k=k->ai_next)
			    {
			        if((csock[i]=socket(k->ai_family,k->ai_socktype,k->ai_protocol))==-1)
			        {
			            perror("client: socket");
			            continue;
			        }

			        if (connect(csock[i],k->ai_addr,k->ai_addrlen)==-1)
			        {
			            close(csock[i]);
			            perror("client: connect");
			            continue;
			        }
			        break;
			    }
			    if(k==NULL)
			    {
			        fprintf(stderr, "client: failed to connect\n");
			        exit(1);
			    }
			    freeaddrinfo(res);
			    
			}
			if(csock[i]!=-1)
			{
				//fcntl(csock[i],F_SETFL, O_NONBLOCK);
				//int flag=fcntl(csock[i],F_GETFL,0);
				int x=fcntl(csock[i],F_SETFL, O_NONBLOCK);
				cout<<"fcntl="<<x<<endl;
				fflush(stdout);
				/*
				if(connect(csock[i],(struct sockaddr *)&csock_sin[i],sizeof(csock_sin[i]))<0)
				{
					fprintf(stderr, "connect failed.\n");
				}
				*/
				fprintf(stderr, "connect OK.\n");
				fprintf(stderr, "%s %s %s csock=%d\n", host[i],port[i],filename[i],csock[i]);
			}
			else //csock==-1
			{
				if(errno != EINPROGRESS)
				{
					fprintf(stderr, "connect error1.\n");
					close(csock[i]);
					return -1;
				}
				else
				{
					fprintf(stderr, "connect error2.\n");
				}
			}
		}
		else
		{
			fprintf(stderr,"File error\n");
		}
	}
	
	int conn=0;
	nfds=FD_SETSIZE;
	FD_ZERO(&rfds);
	FD_ZERO(&wfds);
	FD_ZERO(&rs);
	FD_ZERO(&ws);
	for(int i=0;i<MAXSER;i++)	//set status
	{
		if(csock[i]!=-1)
		{
			FD_SET(csock[i],&rs);
			FD_SET(csock[i],&ws);
			status[i]=F_CONNECTING;
			conn++;
		}
	}
	fprintf(stderr,"==========conn=%d===========\n",conn);
	rfds=rs;
	wfds=ws;
	while(conn>0)
	{
		memcpy(&rfds,&rs,sizeof(rfds));
		memcpy(&wfds,&ws,sizeof(wfds));
		if(select(nfds,&rfds,&wfds,(fd_set*)0,(struct timeval *)0) < 0)
		{
			fprintf(stderr,"select error\n");
			return -1;
		}
		else
		{
			fprintf(stderr,"select ok\n");
		}
		for(int i=0;i<MAXSER;i++)
		{
			if(status[i]==F_CONNECTING && 
				(FD_ISSET(csock[i],&rfds) || FD_ISSET(csock[i],&wfds)))
			{
				fprintf(stderr,"csock_CONNECT=%d\n",csock[i]);
				socklen_t elen=sizeof(int);
				if(getsockopt(csock[i],SOL_SOCKET,SO_ERROR,&errno,&elen)<0 || errno!=0)
				{
					//non-blocking connect failed
					fprintf(stderr,"getsockopt error\n");
					return -1;
				}
				else
				{
					fprintf(stderr,"getsockopt OK\n");
				}
				status[i]=F_READING;
				FD_CLR(csock[i],&ws);
			}
			else if (status[i]==F_WRITING && FD_ISSET(csock[i],&wfds))
			{
				fprintf(stderr,"csock_WRITING=%d\n",csock[i]);
				char command[MAXLINE] = {0};
				int n=readline(filefd[i],command,MAXLINE,i);
				fprintf(stderr,"%s",command);
				write(csock[i],command,strlen(command));
				cout<<"<script>document.all['m"<<i<<"'].innerHTML += \"% <b>";
				for(int j=0;j<MAXLINE;j++)
				{
					if(command[j]=='\0'){break;}
					switch(command[j])
					{
						case '>':
			                cout<<"&gt;";
			                break;
			            case '<':
			                cout<<"&lt;";
			                break;
			            case '\r':
			                break;
			            case '\n':
			                break;
			            case '"':
			                cout<<"&quot;";
			                break;
			            default:
			                cout<<command[j];
			                break;
					}
				}
				cout<<"</b><br>\";</script>\r\n";
				fflush(stdout);
				status[i]=F_READING;
				FD_CLR(csock[i],&ws);
				FD_SET(csock[i],&rs);
			}
			else if(status[i]==F_READING && FD_ISSET(csock[i],&rfds))
			{
				char result[MAXLINE]={0};
				fprintf(stderr,"csock_READING=%d\n",csock[i]);
				int n=readline(csock[i],result,MAXLINE,i);
				fprintf(stderr,"%s",result);
				if(n<=0)
				{
					//read finished
					status[i]=F_DONE;
					FD_CLR(csock[i],&rs);
					conn--;
					close(csock[i]);
					continue;
				}
				if(gSc[i]==1)	//%_
				{
					fprintf(stderr,"GSC=1\n");
					status[i]=F_WRITING;
					FD_CLR(csock[i],&rs);
					FD_SET(csock[i],&ws);
					gSc[i]=0;
				}
				else
				{
					fprintf(stderr,"GSC=0\n");
					cout<<"<script>document.all['m"<<i<<"'].innerHTML += \"";
					for(int j=0;j<MAXLINE;j++)
					{
						if(result[j]=='\0'){break;}
						switch(result[j])
						{
							case '>':
				                cout<<"&gt;";
				                break;
				            case '<':
				                cout<<"&lt;";
				                break;
				            case '\r':
				                break;
				            case '\n':
				                break;
				            case '"':
				                cout<<"&quot;";
				                break;
				            default:
				                cout<<result[j];
				                break;
						}
					}
					//fflush(stdout);
					cout<<"<br>\";</script>\r\n";
					fflush(stdout);
				}
			}
		}
	}
	cout<<"</font>\n";
    cout<<"</body>\n";
  	cout<<"</html>\n";
  	fflush(stdout);

	return 0;
}